import openpyxl as xl

def processSheetAndCreateWorkbook(sheetObj):
    # open new workbook
    wb_new = xl.Workbook()
  
    # create a new sheet
    ws_new =  wb_new.active

    #give a title to the sheet
    ws_new.title = sheetObj.title + '_1'
   
    # copy the content of the worksheet into the worksheet of the new workbook
    for row in sheetObj:
        for cell in row:
            ws_new[cell.coordinate].value = cell.value

    # save the workbook
    wb_new.save(filename = sheetObj.title + '.xlsx')



if __name__ == "__main__": 
    # open a workbook
    wb2 = xl.load_workbook('workbook.xlsx')

    list_of_sheets = wb2.get_sheet_names()



    for sheet in list_of_sheets:
        sheetObj = wb2.get_sheet_by_name(sheet)

        processSheetAndCreateWorkbook(sheetObj)
    

